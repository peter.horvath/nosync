#!/usr/bin/make -f
CC:=gcc

.PHONY: default
default: lib/libnosync.so

lib/libnosync.so: src/nosync.c
	$(CC) -shared -s -o $@ $<

.PHONY: clean
clean:
	$(RM) -vf lib/nosync.so
