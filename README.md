Trivial wrapper to disable unneeded fsync()/fdatasync() calls

Simple wrapper library. Usage:

```
LD_PRELOAD=nosync.so /your/binary
```

It will execute your binary without the capability of calling fsync()/fdatasync().
/usr/bin/nosync /your/binary does the same as a tool.
