#define __USE_LARGEFILE64
#define _GNU_SOURCE

#include <fcntl.h>
#include <sys/types.h>

int msync(void *addr, size_t length, int flags) {
  return 0;
}     

int sync() {
  return 0;
}

int syncfs(int fd) {
  return 0;
}

int fsync(int fd) {
  return 0;
}

int fdatasync(int fd) {
  return 0;
}

int sync_file_range(int fd, off64_t offset, off64_t nbytes, unsigned int flags) {
  return 0;
}
